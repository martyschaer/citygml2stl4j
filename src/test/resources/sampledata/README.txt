Unfortunately we are not allowed to include the test data provided by SwissTopo.
You can get it here: https://cms.geo.admin.ch/Topo/swissbuildings3d2/Sample_swissBUILDINGS3D2.0_LV95.zip

Just include the two CityGML *.gml files in this directory.