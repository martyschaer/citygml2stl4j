package dev.schaer.citygml2stl4j;

import dev.schaer.citygml2stl4j.model.City;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;

class ImporterTest {

    private static final String FILE_A_NAME = "Sample_swissBUILDINGS3D20_LV95_1166-42.gml";
    private static final String FILE_B_NAME = "Sample_swissBUILDINGS3D20_LV95_1166-44.gml";

    private File getSampleFromResources(final String name) {
        return Helper.getFileFromResources("sampledata/" + name);
    }

    @Test
    void importFileA() throws Exception{
        var aFile = getSampleFromResources(FILE_A_NAME);
        Importer importer = new Importer();
        City city = importer.importFile(aFile);
        assertEquals(1504, city.getBuildings().size());
    }

    @Test
    void importFileB() throws Exception{
        var aFile = getSampleFromResources(FILE_B_NAME);
        Importer importer = new Importer();
        City city = importer.importFile(aFile);
        assertEquals(112, city.getBuildings().size());
    }
}