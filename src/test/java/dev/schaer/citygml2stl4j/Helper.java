package dev.schaer.citygml2stl4j;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

public class Helper {
    public static byte[] getBytesFromResources(final String resourceName) {
        try {
            return Files.readAllBytes(getFileFromResources(resourceName).toPath());
        } catch (IOException e) {
            e.printStackTrace();
            fail(e);
        }
        return null;
    }

    public static File getFileFromResources(final String resourceName) {
        try {
            var resource = Helper.class.getClassLoader().getResource(resourceName);
            assertNotNull(resource);
            return new File(resource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail(e);
        }
        return null;
    }
}
