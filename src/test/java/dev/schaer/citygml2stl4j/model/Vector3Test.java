package dev.schaer.citygml2stl4j.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector3Test {
    @Test
    void testToStlString() {
        var value = new Vector3(10.0f, 0.42f, -13.37f);
        assertEquals("vertex 1.25e3 1.68e-2 -1.67125e3", value.toStlString());
    }

    @Test
    void testToStlStringZero() {
        var value = new Vector3(0.0f, 0.0f, 0.0f);
        assertEquals("vertex 0 0 0", value.toStlString());
    }

    @Test
    void testSub() {
        var a = new Vector3(12, 0, 2);
        var b = new Vector3(4, 0, 5);

        var result = a.sub(b);

        var expected = new Vector3(8, 0, -3);

        assertEquals(expected, result);
    }

    @Test
    void testNormalize() {
        var a = new Vector3(3, 4, 0);

        var result = a.normalize();

        var expected = new Vector3(0.6f, 0.8f, 0);

        assertEquals(expected, result);
    }

    @Test
    void testCrossProduct() {
        var a = new Vector3(2f, 3f, 4f);
        var b = new Vector3(5f, 6f, 7f);

        var result = a.cross(b);

        var expected = new Vector3(-3, 6, -3);

        assertEquals(expected, result);
    }
}