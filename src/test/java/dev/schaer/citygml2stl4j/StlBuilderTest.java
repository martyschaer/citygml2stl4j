package dev.schaer.citygml2stl4j;

import dev.schaer.citygml2stl4j.model.Triangle;
import dev.schaer.citygml2stl4j.model.Vector3;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class StlBuilderTest {
    private static final float zero = 0.0f;
    private static final float height = 15.0f;
    private static final float unit = (float) Math.sqrt(height * height + height * height);
    private static final Vector3 A = new Vector3(-unit/2, unit/2, zero);
    private static final Vector3 B = new Vector3(-unit/2, unit/2, height);
    private static final Vector3 C = new Vector3(zero, zero, zero);
    private static final Vector3 D = new Vector3(zero, unit, zero);

    @Test
    void testBasicPyramid() {
        StlBuilder builder = new StlBuilder();
        builder.triangle(new Triangle(B, C, D));
        builder.triangle(new Triangle(A, B, D));
        builder.triangle(new Triangle(A, C, B));
        builder.triangle(new Triangle(A, D, C));
        var actual = builder.finish();

        var expected = Helper.getBytesFromResources("test_binary.stl");

        assertArrayEquals(expected, actual);

        /*
        try(FileOutputStream fos = new FileOutputStream("/tmp/test.stl")) {
            fos.write(actual);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e);
        }

         */
    }
}