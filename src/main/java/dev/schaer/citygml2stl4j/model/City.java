package dev.schaer.citygml2stl4j.model;

import org.citygml4j.model.citygml.building.Building;

import java.util.List;

public class City {
    private final List<Building> buildings;
    private final Pair<Vector3> bounds;

    public City(List<Building> buildings, Pair<Vector3> bounds) {
        this.buildings = buildings;
        this.bounds = bounds;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public Pair<Vector3> getBounds() {
        return bounds;
    }
}
