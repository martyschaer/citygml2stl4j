package dev.schaer.citygml2stl4j.model;

import java.util.Objects;

public class Vector3 {
    private final float x;
    private final float y;
    private final float z;

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public Vector3 sub(Vector3 other) {
        return new Vector3(
                this.x - other.x,
                this.y - other.y,
                this.z - other.z
        );
    }

    public Vector3 cross(Vector3 other) {
        return new Vector3(
                this.y * other.z - this.z * other.y,
                this.z * other.x - this.x * other.z,
                this.x * other.y - this.y * other.x
        );
    }

    public Vector3 normalize() {
        var length = (float) Math.sqrt(x * x + y * y + z * z);
        return new Vector3(
                x / length,
                y / length,
                z / length
        );
    }

    public String toStlString() {
        return String.format("vertex %s %s %s",
                formatFloat(x),
                formatFloat(y),
                formatFloat(z));
    }

    private String formatFloat(float f) {
        if (f == 0) {
            return "0";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(getMantissa(f));
        sb.append("e");
        sb.append(getExponent(f));
        return sb.toString();
    }

    private float getMantissa(float x) {
        return (float) (x / Math.pow(2, getExponent(x)));
    }

    private int getExponent(float x) {
        return Math.getExponent(x);
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3 vector3 = (Vector3) o;

        if (!Objects.equals(x, vector3.x)) return false;
        if (!Objects.equals(y, vector3.y)) return false;
        return Objects.equals(z, vector3.z);
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        return result;
    }
}
