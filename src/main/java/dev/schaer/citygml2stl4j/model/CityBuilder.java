package dev.schaer.citygml2stl4j.model;

import org.citygml4j.model.citygml.building.Building;

import java.util.ArrayList;
import java.util.List;

public class CityBuilder {
    private final List<Building> buildings = new ArrayList<>();
    private Vector3 upperCorner = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 lowerCorner = new Vector3(0.0f, 0.0f, 0.0f);

    public CityBuilder building(final Building building) {
        buildings.add(building);
        return this;
    }

    public CityBuilder upperCorner(List<Double> values) {
        assert values.size() == 3;
        upperCorner = new Vector3( //
                values.get(0).floatValue(), //
                values.get(1).floatValue(), //
                values.get(2).floatValue() //
        );
        return this;
    }

    public CityBuilder lowerCorner(List<Double> values) {
        assert values.size() == 3;
        lowerCorner = new Vector3( //
                values.get(0).floatValue(), //
                values.get(1).floatValue(), //
                values.get(2).floatValue() //
        );
        return this;
    }

    public City build() {
        return new City(buildings, new Pair<>(upperCorner, lowerCorner));
    }
}
