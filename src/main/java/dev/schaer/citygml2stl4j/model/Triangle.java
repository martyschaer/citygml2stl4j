package dev.schaer.citygml2stl4j.model;

public class Triangle {
    public final Vector3 vertexA;
    public final Vector3 vertexB;
    public final Vector3 vertexC;
    public final Vector3 normal;

    public Triangle(Vector3 vertexA, Vector3 vertexB, Vector3 vertexC) {
        this.vertexA = vertexA;
        this.vertexB = vertexB;
        this.vertexC = vertexC;
        this.normal = vertexB.sub(vertexA).cross(vertexC.sub(vertexA)).normalize();
    }

    public String toStlString() {
        return String.format("""
                facet normal 0 0 0
                    outer loop
                        %s
                        %s
                        %s
                    endloop
                endfacet
                """, vertexA.toStlString(), vertexB.toStlString(), vertexC.toStlString());
    }
}
