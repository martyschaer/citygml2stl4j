package dev.schaer.citygml2stl4j;

import dev.schaer.citygml2stl4j.model.Triangle;
import dev.schaer.citygml2stl4j.model.Vector3;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StlBuilder {
    private static final int BYTES_PER_TRIANGLE = 50;
    private static final byte[] HEADER = ("MESH-".repeat(15) + "MESH\n").getBytes(StandardCharsets.UTF_8);
    private static final byte[] ATTRIBUTE_BYTE_COUNT = new byte[2];
    private final List<Triangle> triangles = new ArrayList<>();

    static {
        // attributes are not supported by most software
        Arrays.fill(ATTRIBUTE_BYTE_COUNT, (byte) 0x00);
    }

    public StlBuilder triangle(Triangle triangle) {
        triangles.add(triangle);;
        return this;
    }

    public byte[] finish() {
        final int outputSize = triangles.size() * BYTES_PER_TRIANGLE + 80 + 4;
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(outputSize);

        outputStream.writeBytes(HEADER);
        writeInt(triangles.size(), outputStream);


        for(Triangle triangle : triangles) {
            writeVector(triangle.normal, outputStream);
            writeVector(triangle.vertexA, outputStream);
            writeVector(triangle.vertexB, outputStream);
            writeVector(triangle.vertexC, outputStream);
            outputStream.writeBytes(ATTRIBUTE_BYTE_COUNT);
        }

        triangles.clear();

        return outputStream.toByteArray();
    }

    private void writeVector(Vector3 data, ByteArrayOutputStream target) {
        writeFloat(data.getX(), target);
        writeFloat(data.getY(), target);
        writeFloat(data.getZ(), target);
    }

    private void writeInt(int data, ByteArrayOutputStream target) {
        target.write(data & 0xff);
        target.write((data >>> 8) & 0xff);
        target.write((data >>> 16) & 0xff);
        target.write((data >>> 24) & 0xff);
    }

    private void writeFloat(Float data, ByteArrayOutputStream target) {
        final var bits = Float.floatToIntBits(data);
        target.write(bits & 0xff);
        target.write((bits >>> 8) & 0xff);
        target.write((bits >>> 16) & 0xff);
        target.write((bits >>> 24) & 0xff);
    }
}
