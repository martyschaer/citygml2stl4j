package dev.schaer.citygml2stl4j;

import dev.schaer.citygml2stl4j.model.City;
import dev.schaer.citygml2stl4j.model.CityBuilder;
import org.citygml4j.CityGMLContext;
import org.citygml4j.builder.jaxb.CityGMLBuilder;
import org.citygml4j.builder.jaxb.CityGMLBuilderException;
import org.citygml4j.model.citygml.CityGML;
import org.citygml4j.model.citygml.CityGMLClass;
import org.citygml4j.model.citygml.building.Building;
import org.citygml4j.model.citygml.core.AbstractCityObject;
import org.citygml4j.model.citygml.core.CityModel;
import org.citygml4j.model.citygml.core.CityObjectMember;
import org.citygml4j.xml.io.CityGMLInputFactory;
import org.citygml4j.xml.io.reader.CityGMLReadException;
import org.citygml4j.xml.io.reader.CityGMLReader;

import java.io.File;

public class Importer {
    public Importer() {

    }

    public City importFile(final File file) throws CityGMLBuilderException, CityGMLReadException {
        CityGMLContext ctx = CityGMLContext.getInstance();
        CityGMLBuilder builder = ctx.createCityGMLBuilder();

        CityGMLInputFactory in = builder.createCityGMLInputFactory();
        CityGMLReader reader = in.createCityGMLReader(file);

        CityBuilder cityBuilder = new CityBuilder();

        while(reader.hasNext()) {
            CityGML gml = reader.nextFeature();

            if(gml.getCityGMLClass() == CityGMLClass.CITY_MODEL) {
                CityModel model = (CityModel) gml;

                cityBuilder.lowerCorner(model.getBoundedBy().getEnvelope().getLowerCorner().getValue());
                cityBuilder.upperCorner(model.getBoundedBy().getEnvelope().getUpperCorner().getValue());

                for(CityObjectMember member : model.getCityObjectMember()) {
                    AbstractCityObject obj = member.getCityObject();
                    if(obj.getCityGMLClass() == CityGMLClass.BUILDING) {
                        cityBuilder.building((Building) obj);
                    } else {
                        System.out.println(obj.getCityGMLClass().name());
                    }
                }
            }
        }

        return cityBuilder.build();
    }
}
