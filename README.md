# citygml2stl4js
This is a project which takes in 3d geo information in the form of CityGML,
such as the [swissBUILDINGS3D 2.0](https://www.swisstopo.admin.ch/en/geodata/landscape/buildings3d2.html)
dataset by the swisstopo,
with the aim of producing STL files suitable for 3D-printing.

Makes use of the following open source libraries:
- [citygml4j](https://github.com/citygml4j/citygml4j)